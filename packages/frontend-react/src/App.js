import { useState } from 'react';
import './App.css';
import Header from "./shared/Header";

function App() {
  return (
    <div className="App">
      <Header />
    </div>
  );
}

export default App;
